package storage

import "errors"

type UserStorage struct {
	userInfo map[string]User
}

func NewUserStorage(userInfo map[string]User) *UserStorage {
	return &UserStorage{
		userInfo: userInfo,
	}
}

func (u *UserStorage) AddUser(user User) error {
	if _, f := u.userInfo[user.UserName]; f {

		return errors.New("user exist")
	}
	u.userInfo[user.UserName] = user
	return nil
}

func (u *UserStorage) UpdateUserWithSessionId(user User) error {
	if _, f := u.userInfo[user.UserName]; f {
		u.userInfo[user.UserName] = user
		return nil

	}
	return errors.New("user do not exist")
}

func (u *UserStorage) GetUserByName(userName string) (User, error) {
	if _, f := u.userInfo[userName]; f {
		return u.userInfo[userName], nil
	}
	return User{}, errors.New("user do not exist")
}

func (u *UserStorage) GetActiveUsers() []string {
	var users []string
	for _, user := range u.userInfo {
		if user.SessionId != "" {
			users = append(users, user.UserName)
		}
	}
	return users
}

func (u *UserStorage) ValidSessionIdCheck(sessionId string) bool {
	for _, user := range u.userInfo {
		if user.SessionId == sessionId {
			return true
		}
	}
	return false

}
