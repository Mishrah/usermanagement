package storage

type User struct {
	UserName  string `json:"userName"`
	Password  string
	SessionId string
}
