package usermanagement

import (
	"errors"
	"math/rand"
	"user-management/internal/storage"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

type UserManagement struct {
	storage *storage.UserStorage
}

func NewUserManagement(storage *storage.UserStorage) *UserManagement {
	return &UserManagement{
		storage: storage,
	}
}

func (um *UserManagement) UserLogin(userName, password string) (string, error) {
	user, err := um.storage.GetUserByName(userName)
	if err != nil {
		return "", err
	}
	if user.Password != password {
		return "", errors.New("password did not match")
	}
	sId := GetSessionId(10)
	user.SessionId = sId
	err = um.storage.UpdateUserWithSessionId(user)
	return sId, nil

}

func (um *UserManagement) UserRegister(user storage.User) error {

	err := um.storage.AddUser(user)
	if err != nil {
		return err
	}
	return nil
}

func (um *UserManagement) FetchActiveUser() []string {
	return um.storage.GetActiveUsers()
}

func (um *UserManagement) ValidSessionIdCheck(s string) bool {
	return um.storage.ValidSessionIdCheck(s)
}

func GetSessionId(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}
