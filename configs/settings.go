package configs

import (
	"log"
	"sync"
	"time"

	"github.com/caarlos0/env/v6"
)

type Settings struct {
	HTTPHost        string        `env:"HTTP_PORT" envDefault:"localhost:8080"`
	DebugHost       string        `env:"DebugHost" envDefault:"localhost:6060"`
	ReadTimeout     time.Duration `env:"READ_TIME_OUT" envDefault:"5s"`
	WriteTimeout    time.Duration `env:"WRITE_TIME_OUT" envDefault:"5s"`
	ShutdownTimeout time.Duration `env:"SHUTDOWN_TIME_OUT" envDefault:"5s"`
}

var (
	settings   Settings
	lockconfig sync.Once
)

func GetSettings() Settings {
	lockconfig.Do(func() {
		settings = newSettings()
	})
	return settings
}

func newSettings() Settings {
	var cfg = Settings{}
	if err := env.Parse(&cfg); err != nil {
		log.Panic(err.Error())
	}

	return cfg
}

func GetHTTPHost() string {
	return GetSettings().HTTPHost
}

func GetDebugHostPort() string {
	return GetSettings().DebugHost
}

func GetReadTimeout() time.Duration {
	return GetSettings().ReadTimeout
}

func GetWriteTimeout() time.Duration {
	return GetSettings().WriteTimeout
}

func GetShutdownTimeout() time.Duration {
	return GetSettings().ShutdownTimeout
}
