package main

import (
	"context"
	"log"
	"net/http"
	_ "net/http/pprof" // Register the pprof handlers
	"os"
	"os/signal"
	"syscall"

	"user-management/cmd/usermanagement-api/handlers"
	"user-management/configs"
	"user-management/internal/domain/usermanagement"
	"user-management/internal/storage"

	"github.com/pkg/errors"
)

func main() {
	if err := run(); err != nil {
		log.Println("shutting down", "error:", err)
		os.Exit(1)
	}
}

func run() error {

	// =========================================================================
	// Logging

	log := log.New(os.Stdout, "	UserController : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	// =========================================================================
	// App Starting

	log.Printf("main : Started")
	defer log.Println("main : Completed")

	go func() {
		log.Println("DebugHost service listening on", configs.GetDebugHostPort())
		err := http.ListenAndServe(configs.GetDebugHostPort(), http.DefaultServeMux)
		log.Println("DebugHost service closed", err)
	}()

	// =========================================================================
	// Start API Service
	userInfo := make(map[string]storage.User)
	db := storage.NewUserStorage(userInfo)
	um := usermanagement.NewUserManagement(db)

	api := http.Server{
		Addr:         configs.GetHTTPHost(),
		Handler:      handlers.API(log, um),
		ReadTimeout:  configs.GetReadTimeout(),
		WriteTimeout: configs.GetWriteTimeout(),
	}

	// Make a channel to listen for errors coming from the listener. Use a
	// buffered channel so the goroutine can exit if we don't collect this error.
	serverErrors := make(chan error, 1)

	// Start the service listening for requests.
	go func() {
		log.Printf("main : API listening on %s", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	// Use a buffered channel because the signal package requires it.
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	// =========================================================================
	// Shutdown

	// Blocking main and waiting for shutdown.
	select {
	case err := <-serverErrors:
		return errors.Wrap(err, "shutting down server")

	case <-shutdown:
		log.Println("main : Start shutdown")

		// Give outstanding requests a deadline for completion.
		ctx, cancel := context.WithTimeout(context.Background(), configs.GetShutdownTimeout())
		defer cancel()

		// Asking listener to shutdown and load shed.
		err := api.Shutdown(ctx)
		if err != nil {
			log.Printf("main : Graceful shutdown did not complete in %v : %v", configs.GetShutdownTimeout(), err)
			err = api.Close()
		}

		if err != nil {
			return errors.Wrap(err, "could not stop server gracefully")
		}
	}

	return nil
}
