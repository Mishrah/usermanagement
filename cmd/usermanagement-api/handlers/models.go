package handlers

type LoginRequest struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
}

type LoginResponse struct {
	SessionId string `json:"sessionId"`
}

type RegisterRequest struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
}

type RegisterResponse struct {
	Msg string `json:"message"`
}
