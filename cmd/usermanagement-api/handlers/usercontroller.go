package handlers

import (
	"errors"
	"net/http"
	"user-management/internal/domain/usermanagement"
	"user-management/internal/storage"
	"user-management/pkg/web"
)

type UserController struct {
	userManagement *usermanagement.UserManagement
}

func NewUserController(userManagement *usermanagement.UserManagement) *UserController {
	return &UserController{
		userManagement: userManagement,
	}
}

func (c *UserController) Login(w http.ResponseWriter, r *http.Request) error {

	var user LoginRequest
	err := web.Decode(r, &user)
	if err != nil {
		return web.RespondError(w, web.NewRequestError(err, http.StatusBadRequest))
	}

	sessionId, err := c.userManagement.UserLogin(user.UserName, user.Password)
	if err != nil {
		return web.RespondError(w, web.NewRequestError(err, http.StatusBadRequest))
	}
	resp := LoginResponse{
		SessionId: sessionId,
	}
	return web.Respond(w, resp, http.StatusOK)
}

func (c *UserController) Register(w http.ResponseWriter, r *http.Request) error {
	var rr RegisterRequest
	err := web.Decode(r, &rr)
	if err != nil {
		return web.RespondError(w, web.NewRequestError(err, http.StatusBadRequest))
	}
	user := storage.User{
		UserName:  rr.UserName,
		Password:  rr.Password,
		SessionId: "",
	}
	err = c.userManagement.UserRegister(user)
	if err != nil {
		return web.RespondError(w, web.NewRequestError(err, http.StatusBadRequest))
	}

	registerResp := RegisterResponse{
		Msg: "user registered successfully",
	}

	return web.Respond(w, registerResp, http.StatusOK)
}

func (c *UserController) GetAllActiveusers(w http.ResponseWriter, r *http.Request) error {

	userSessionId := r.Header.Get("sessionId")
	if userSessionId == "" {
		return web.RespondError(w, web.NewRequestError(errors.New("please enter session Id in header"), http.StatusUnauthorized))
	}
	if !c.userManagement.ValidSessionIdCheck(userSessionId) {
		return web.RespondError(w, web.NewRequestError(errors.New("invalid SessionId"), http.StatusBadRequest))
	}

	activeUsers := c.userManagement.FetchActiveUser()

	return web.Respond(w, activeUsers, http.StatusOK)
}
