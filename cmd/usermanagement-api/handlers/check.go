package handlers

import (
	"net/http"
	"user-management/pkg/web"
)

// Check provides support for orchestration health checks.
type Check struct {
	// ADD OTHER STATE LIKE THE LOGGER IF NEEDED.
}

// Health validates the service is healthy and ready to accept requests.
func (c *Check) Health(w http.ResponseWriter, r *http.Request) error {

	var health struct {
		Status string `json:"status"`
	}

	health.Status = "ok"
	return web.Respond(w, health, http.StatusOK)
}
