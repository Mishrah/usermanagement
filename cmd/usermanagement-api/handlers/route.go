package handlers

import (
	"log"
	"net/http"
	"user-management/internal/domain/usermanagement"
	"user-management/pkg/web"
)

func API(log *log.Logger, um *usermanagement.UserManagement) http.Handler {

	app := web.NewApp(log)

	{
		c := Check{}
		app.Handle(http.MethodGet, "/v1/health", c.Health)
	}

	{
		um := NewUserController(um)
		app.Handle(http.MethodPost, "/register", um.Register)
		app.Handle(http.MethodPost, "/login", um.Login)
		app.Handle(http.MethodGet, "/getusers", um.GetAllActiveusers)

	}

	return app
}
