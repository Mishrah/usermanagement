module user-management

go 1.15

require (
	github.com/caarlos0/env/v6 v6.5.0
	github.com/go-chi/chi v1.5.4
	github.com/pkg/errors v0.9.1
)
